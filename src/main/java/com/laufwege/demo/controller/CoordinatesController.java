package com.laufwege.demo.controller;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.laufwege.demo.dao.CoordinatesRepo;
import com.laufwege.demo.dao.SessionRepo;
import com.laufwege.demo.dao.UserRepo;
import com.laufwege.demo.model.Coordinates;
import com.laufwege.demo.model.Session;

@RestController
public class CoordinatesController {

	@Autowired
	CoordinatesRepo coordinatesRepo;

	@Autowired
	SessionRepo sessionRepo;
	
	@Autowired
	UserRepo userRepo;
	
	@PostMapping(path = "coordinates")
	public Coordinates addCoordinates(@RequestBody Coordinates coordinates) {

		coordinatesRepo.save(coordinates);

		return coordinates;
	}
	
	
	@PostMapping(path = "session/{sessionId}/coordinates")
	public void addCoordinatesList(@RequestBody List<Coordinates> coordinates, @PathVariable("sessionId") int sessionId) {
		
		for(Coordinates coord : coordinates) {
			
			coord.setSession(sessionRepo.getOne(sessionId));
			
		}
		
		coordinatesRepo.saveAll(coordinates);

	}
	
	
	@GetMapping(path = "coordinates")
	public List<Coordinates> getAllCoordinates() {

		return coordinatesRepo.findAll();

	}

	@GetMapping(path = "coordinates/{coordinatesId}")
	public Optional<Coordinates> getCoordinates(@PathVariable("coordinatesId") int coordinatesId) {

		// ModelAndView mv = new ModelAndView("showSpecificBeacon.jsp");
		// repo.findById(beaconId).map(mv::addObject);

		return coordinatesRepo.findById(coordinatesId);
	}

	
	@GetMapping(path = "session/{sessionId}/coordinates")
	public List<Coordinates> getCoordinatesForSession(@PathVariable("sessionId") int sessionId) {

		Optional<Session> session = sessionRepo.findById(sessionId);
		List<Coordinates> coordinatesList = new ArrayList<>();
		
		if(session.isPresent()) {
			coordinatesList = coordinatesRepo.findBySession(session.get());
		}
		
		return coordinatesList;
	}
	
	@GetMapping(path="user/{userId}/coordinates")
	public List<Coordinates> getCoordinatesForUser(@PathVariable("userId") int userId) {
		
		List<Session> sessionList = sessionRepo.findByUser(userRepo.getOne(userId));
		
		List<Coordinates> coordinates = new ArrayList<>();
		
		for(Session session : sessionList) {
			coordinates.addAll(coordinatesRepo.findBySession(session));
		}
		
		return coordinates;
	}
	
	
	@GetMapping(path="user/{userId}/startTime/{startTime}/endTime/{endTime}/coordinates")
	public List<Coordinates> getCoordinatesForUserByTimestamps(@PathVariable("userId") int userId, @PathVariable ("startTime") String startTime, @PathVariable("endTime") String endTime) {
		
		LocalDateTime startDateTime = LocalDateTime.parse(startTime, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
		LocalDateTime endDateTime = LocalDateTime.parse(endTime, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
		
		return coordinatesRepo.getCoordinatesForUserByTime(userId, startDateTime, endDateTime);
	
		
	}
	
	
	@DeleteMapping(path = "coordinates/{coordinatesId}")
	public String deleteBeacon(@PathVariable("coordinatesId") int coordinatesId) {

		Coordinates coordinates = coordinatesRepo.getOne(coordinatesId);
		coordinatesRepo.delete(coordinates);

		return "Deleted";
	}

	@Transactional
	@DeleteMapping(path = "session/{sessionId}/coordinates")
	public String deleteBeaconsOfSession(@PathVariable("sessionId") int sessionId) {

		//List<Coordinates> coordinates = coordinatesRepo.findBySession(sessionRepo.);
		
		
		coordinatesRepo.deleteAllBySession(sessionRepo.getOne(sessionId));

		return "Deleted";
	}
	
	/*
	 * @PutMapping(path = "coordinates") public Coordinates
	 * updateCoordinates(@RequestBody Coordinates coordinates) {
	 * 
	 * coordinatesRepo.save(coordinates);
	 * 
	 * return coordinates; }
	 */
	
	
	
}
