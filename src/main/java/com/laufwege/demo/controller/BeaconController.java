package com.laufwege.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.laufwege.demo.dao.BeaconRepo;
import com.laufwege.demo.model.Beacon;

@RestController
public class BeaconController {

	@Autowired
	BeaconRepo repo;

	@PostMapping(path = "beacon")
	public Beacon addBeacon(@RequestBody Beacon beacon) {

		repo.save(beacon);

		return beacon;
	}

	@GetMapping(path = "beacon")
	public List<Beacon> getAllBeacons() {

		return repo.findAll();

	}

	@GetMapping(path = "beacon/{beaconId}")
	public Optional<Beacon> getBeacon(@PathVariable("beaconId") String beaconId) {

		// ModelAndView mv = new ModelAndView("showSpecificBeacon.jsp");

		// repo.findById(beaconId).map(mv::addObject);

		return repo.findById(beaconId);
	}

	@DeleteMapping(path = "beacon/{beaconId}")
	public String deleteBeacon(@PathVariable("beaconId") String beaconId) {

		Beacon beacon = repo.getOne(beaconId);
		repo.delete(beacon);

		return "Deleted";
	}

	@PutMapping(path = "beacon")
	public Beacon updateBeacon(@RequestBody Beacon beacon) {

		repo.save(beacon);

		return beacon;
	}

}
