package com.laufwege.demo.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.laufwege.demo.dao.SessionRepo;
import com.laufwege.demo.dao.UserRepo;
import com.laufwege.demo.model.Session;
import com.laufwege.demo.model.User;

@RestController
public class UserController {

	@Autowired
	UserRepo userRepo;
	
	@Autowired
	SessionRepo sessionRepo;
	
	@PostMapping(path = "user")
	public User addUser(@RequestBody User user) {

		userRepo.save(user);

		return user;
	}

	@GetMapping(path = "user")
	public List<User> getAllUsers() {

		return userRepo.findAll();

	}

	@GetMapping(path = "user/{userId}")
	public Optional<User> getUser(@PathVariable("userId") int userId) {

		// ModelAndView mv = new ModelAndView("showSpecificBeacon.jsp");

		// repo.findById(beaconId).map(mv::addObject);

		return userRepo.findById(userId);
	}
	
	@GetMapping(path = "startTime/{startTime}/user")
	public List<User> getUserByDate(@PathVariable("startTime") String startTime) {
		
		LocalDateTime startDateTime = LocalDateTime.parse(startTime, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
		LocalDateTime endDateTime = startDateTime.plusDays(1);
		
		return userRepo.getUsersBySessionsForDate(startDateTime, endDateTime);
		
	}
	
	
	@GetMapping(path = "session/{sessionId}/user")
	public User getUserBySession(@PathVariable("sessionId") int sessionId) {
		
		Session session = sessionRepo.findById(sessionId).orElse(null);

		
		if(session == null) {
			return null;
		}
		
		return userRepo.findById(session.getUser().getUserId()).orElse(null);
		
	}

	@DeleteMapping(path = "user/{userId}")
	public String deleteBeacon(@PathVariable("userId") int userId) {

		User user= userRepo.getOne(userId);
		userRepo.delete(user);

		return "Deleted";
	}

	@PutMapping(path = "user")
	public User updateUser(@RequestBody User user) {

		userRepo.save(user);

		return user;
	}
	
	
}
