package com.laufwege.demo.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.laufwege.demo.dao.SessionRepo;
import com.laufwege.demo.dao.UserRepo;
import com.laufwege.demo.model.Session;

@RestController
public class SessionController {

	
	@Autowired
	SessionRepo sessionRepo;
	
	@Autowired
	UserRepo userRepo;
	
	@PostMapping(path = "user/{userId}/session")
	public Session addSession(@RequestBody Session session, @PathVariable("userId") int userId) {
		
		session.setUser(userRepo.getOne(userId));
		
		sessionRepo.save(session);
		
		return session;
	}

	@GetMapping(path = "session")
	public List<Session> getAllSessions() {

		return sessionRepo.findAll();

	}

	@GetMapping(path = "user/{userId}/session")
	public List<Session> getAllSessionsforUser(@PathVariable("userId") int userId) {

		return sessionRepo.findByUser(userRepo.getOne(userId));

	}
	
	
	@GetMapping(path = "session/{sessionId}")
	public Optional<Session> getSession(@PathVariable("sessionId") int sessionId) {

		return sessionRepo.findById(sessionId);
	}
	
	
	@GetMapping(path = "startTime/{startTime}/user/{userId}/session")
	public List<Session> getSessionsforUserByDate(@PathVariable("startTime") String startTime, @PathVariable("userId") int userId){
		
		LocalDateTime startDateTime = LocalDateTime.parse(startTime, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
		LocalDateTime endDateTime = startDateTime.plusDays(1);
		
		return sessionRepo.getSessionsForUserByDate(startDateTime, endDateTime, userId);
	
	}
	

	@DeleteMapping(path = "session/{sessionId}")
	public String deleteSession(@PathVariable("sessionId") int sessionId) {

		Session session = sessionRepo.getOne(sessionId);
		sessionRepo.delete(session);

		return "Deleted";
	}

	/*
	 * @PutMapping(path = "/session") public Session updateSession(@RequestBody
	 * Session session) {
	 * 
	 * sessionRepo.save(session);
	 * 
	 * return session; }
	 */
	
}
