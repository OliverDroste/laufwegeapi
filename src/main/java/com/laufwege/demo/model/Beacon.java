package com.laufwege.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="beacon", schema="mydbproject")
public class Beacon{

	@Id
	@Column(name = "beacon_id")
	private String beaconId;
	private double xCoordinate;
	private double yCoordinate;
	private boolean isMachine;
	
	public String getBeaconId() {
		return beaconId;
	}
	
	public void setBeaconId(String beaconID) {
		this.beaconId = beaconID;
	}
	public double getxCoordinate() {
		return xCoordinate;
	}
	public void setxCoordinate(double xCoordinate) {
		this.xCoordinate = xCoordinate;
	}
	public double getyCoordinate() {
		return yCoordinate;
	}
	public void setyCoordinate(double yCoordinate) {
		this.yCoordinate = yCoordinate;
	}
	
	public boolean getIsMachine() {
		return isMachine;
	}

	public void setIsMachine(boolean isMachine) {
		this.isMachine = isMachine;
	}

	@Override
	public String toString() {
		return "Beacon [beaconID=" + beaconId + ", xCoordinate=" + xCoordinate + ", yCoordinate=" + yCoordinate
				+ ", isMachine=" + isMachine + "]";
	}
	
}
