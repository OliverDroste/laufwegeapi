package com.laufwege.demo.model;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="coordinates", schema="mydbproject")
public class Coordinates {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int coordinatesId;
	private Timestamp timestamp;
	private int xCoordinate;
	private int yCoordinate;
	private boolean isMachine;

	@JsonBackReference
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "session_id", nullable = false)
    private Session session;
	
	public Coordinates() {
		
	}

	public Coordinates(int coordinatesId, Timestamp timestamp, int xCoordinate, int yCoordinate, boolean isMachine, Session session) {
		super();
		this.coordinatesId = coordinatesId;
		this.timestamp = timestamp;
		this.xCoordinate = xCoordinate;
		this.yCoordinate = yCoordinate;
		this.isMachine=isMachine;
		this.session = session;
	}

	public int getCoordinatesId() {
		return coordinatesId;
	}

	public void setCoordinatesId(int coordinatesId) {
		this.coordinatesId = coordinatesId;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	public int getxCoordinate() {
		return xCoordinate;
	}

	public void setxCoordinate(int xCoordinate) {
		this.xCoordinate = xCoordinate;
	}

	public int getyCoordinate() {
		return yCoordinate;
	}

	public void setyCoordinate(int yCoordinate) {
		this.yCoordinate = yCoordinate;
	}

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}

	public boolean getIsMachine() {
		return isMachine;
	}

	public void setIsMachine(boolean isMachine) {
		this.isMachine = isMachine;
	}
	
	
	
}
