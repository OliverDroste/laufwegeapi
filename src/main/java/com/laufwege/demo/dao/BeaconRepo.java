package com.laufwege.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.laufwege.demo.model.Beacon;

public interface BeaconRepo extends JpaRepository<Beacon, String>{
	
	/*
	 * List<Beacon> findByIsMachine(Boolean isMachine);
	 * 
	 * @Query("from Beacon where isMachine=?1 order by xCoordinate") List<Beacon>
	 * findByIsMachineSorted(Boolean isMachine);
	 */
	
}
