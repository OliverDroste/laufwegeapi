package com.laufwege.demo.dao;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.laufwege.demo.model.User;

public interface UserRepo  extends JpaRepository<User, Integer>{

	@org.springframework.data.jpa.repository.Query(value= "Select users.user_id, users.name, users.surname "
			+ "From users, sessions Where sessions.start_time >= :startTime "
			+ "AND sessions.start_time < :endTime "
			+ "AND sessions.user_id=users.user_id "
			+ "Group By users.user_id", nativeQuery = true)
	public List<User> getUsersBySessionsForDate(@Param("startTime") LocalDateTime startTime, @Param("endTime") LocalDateTime endTime);
	
	
	
	
}
