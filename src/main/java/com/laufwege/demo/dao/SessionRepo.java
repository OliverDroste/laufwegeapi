package com.laufwege.demo.dao;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.laufwege.demo.model.Session;
import com.laufwege.demo.model.User;

public interface SessionRepo  extends JpaRepository<Session, Integer>{
	
	List<Session> findByUser(User user);
	
	
	@org.springframework.data.jpa.repository.Query(value= "Select sessions.session_id, sessions.user_id, sessions.start_time, sessions.end_time " + 
			"From users, sessions " +
			"Where sessions.start_time >= :startTime " + 
			"AND sessions.start_time < :endTime " +
			"AND sessions.user_id = :userId " + 
			"AND sessions.user_id = users.user_id"
			, nativeQuery = true)
	public List<Session> getSessionsForUserByDate(@Param("startTime") LocalDateTime startTime, @Param("endTime") LocalDateTime endTime, @Param("userId") int userId);
	
	
}
