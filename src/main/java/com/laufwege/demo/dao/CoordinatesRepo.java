package com.laufwege.demo.dao;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;


import com.laufwege.demo.model.Coordinates;
import com.laufwege.demo.model.Session;

public interface CoordinatesRepo  extends JpaRepository<Coordinates, Integer>{

	public List<Coordinates> findBySession(Session session);
	
	public void deleteAllBySession(Session session);
	
	
	
	@org.springframework.data.jpa.repository.Query(value= "Select coordinates.coordinates_id, coordinates.timestamp, coordinates.x_coordinate, coordinates.y_coordinate,is_machine,coordinates.session_id\r\n" + 
			"From coordinates, sessions, users \r\n" + 
			"Where users.user_id= :userId \r\n" + 
			"AND coordinates.timestamp >= :startTime \r\n" + 
			"AND coordinates.timestamp < :endTime \r\n" + 
			"AND sessions.user_id=users.user_id\r\n" + 
			"AND coordinates.session_id=sessions.session_id;", 
			nativeQuery = true)
	public List<Coordinates> getCoordinatesForUserByTime(@Param("userId") int userId, @Param("startTime") LocalDateTime startTime, @Param("endTime") LocalDateTime endTime);
	
}
